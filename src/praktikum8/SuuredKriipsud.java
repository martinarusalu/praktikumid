package praktikum8;
import java.util.*;

public class SuuredKriipsud {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String tekst = sc.nextLine();
		sc.close();
		tekst = tekst.toUpperCase();
		StringBuffer teineTekst = new StringBuffer();
		for (int i = 0; i < tekst.length(); i++) {
			if (i < tekst.length() - 1) teineTekst.append(tekst.charAt(i) + "-");
			else teineTekst.append(tekst.charAt(i));
		}
		System.out.println(teineTekst.toString());
	}

}
