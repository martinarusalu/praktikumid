package praktikum8;
import java.util.*;
import lib.TextIO;

public class NimedVanused {

	public static void main(String[] args) {
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		
		String nimi = "default";
		int vanus = 0;
		
		while (true) {
			System.out.println("Sisesta nimi:");
			nimi = TextIO.getlnString();
			if (nimi.equals("")) break;
			System.out.println("Sisesta vanus:");
			vanus = TextIO.getlnInt();
			inimesed.add(new Inimene(nimi, vanus));
		}

		for (Inimene inimene : inimesed) {
		    inimene.tervita();
		}
	}

}
