package praktikum8;
import lib.TextIO;

public class Podramaja {

	public static void main(String[] args) {
		System.out.println("Millise tähega?");
		char taht = TextIO.getlnChar();
		System.out.printf(genereeriLaul(taht));
	}
	
	public static String genereeriLaul (char taht) {
		String laul = "Põdral maja metsa sees\n"
				+ "Väiksest aknast välja vaatab\n"
				+ "Jänes jookseb kõigest väest\n"
				+ "Lävel seisma jääb\n"
				+ "Kopp kopp lahti tee\n"
				+ "Metsas kuri jahimees\n"
				+ "Jänes tuppa tule sa\n"
				+ "Anna käppa ka";
		laul = vahetaTaishaalikud (laul,taht);
		return laul;
	}
	
	public static String vahetaTaishaalikud (String laul, char taht) {
		for (int i = 0; i < laul.length(); i++) {
			char tahtLaulus = laul.charAt(i);
			if (kasTaishaalik(tahtLaulus)) laul = laul.replace(tahtLaulus, taht);
		}
		return laul;
	}
	
	public static boolean kasTaishaalik (char taht) {
		switch (Character.toLowerCase(taht)) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'õ':
			case 'ä':
			case 'ö':
			case 'ü':
				return true;
			default:
				return false;
		}
	}

}
