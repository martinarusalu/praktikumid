package praktikum8;
import java.util.*;

public class Palindroom {

	public static void main(String[] args) {
		System.out.println("Sisesta sõna:");
		Scanner sc = new Scanner(System.in);
		String sona = sc.nextLine();
		while (sona.equals("")) {
			System.out.println("Sa ei sisestanud sõna! Proovi uuesti:");
			sona = sc.nextLine();
		}
		sc.close();
		if (onPalindroom(sona)) System.out.println("On palindroom.");
		else System.out.println("Ei ole palindroom.");
	}
	
	public static boolean onPalindroom(String sona) {
		String tagurpidi = new StringBuilder(sona).reverse().toString();
		
		if (sona.equals(tagurpidi)) return true;
		else return false;
	}

}
