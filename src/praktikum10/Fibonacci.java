package praktikum10;

public class Fibonacci {

	public static void main(String[] args) {
		for (int i = 10; i > 0; i++) {
			System.out.println(fibonacci(i) + " - " + i);
		}
	}
	
	public static long fibonacci (int arv) {
		if (arv == 0) return 0;
		else if (arv == 1) return 1;
		else return fibonacci(arv-1) + fibonacci(arv-2);
	}

}
