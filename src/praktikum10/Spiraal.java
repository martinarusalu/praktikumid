package praktikum10;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

/**
 * Ringjoone valemi jÃ¤rgi ringi joonistamise nÃ¤ide
 * @author Mikk Mangus
 */
@SuppressWarnings("serial")
public class Spiraal extends Applet {

    private Graphics g;
    
    public void paint(Graphics g) {
        this.g = g;
        joonistaTaust();
        joonistaSpiraal(10,20);
    }
    
    /**
     * Katab tausta valgega
     */
    public void joonistaTaust() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
    
    /**
     * Joonistab ringi
     */
    public void joonistaSpiraal(int raadius, int ringideArv) {
        g.setColor(Color.black);
        int keskkohtX = getWidth() / 2;
        int keskkohtY = getHeight() / 2;
        
        for (double nurk = 0; nurk <= Math.PI * 2 * ringideArv; nurk = nurk + .03) {
            int x = (int) ((raadius + nurk) * Math.cos(nurk));
            int y = (int) ((raadius + nurk) * Math.sin(nurk));
            g.fillRect(keskkohtX + x, keskkohtY + y, 2, 2);
        }
    }
}
