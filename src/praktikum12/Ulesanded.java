package praktikum12;

public class Ulesanded {

	public static void main(String[] args) {
		int [][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
		};
		
		System.out.println("tryki()");
		for (int i = 0; i < neo.length; i++) {
			tryki(neo[i]);
		}
		
		System.out.println("trykiKahene()");
		trykiKahene(neo);
		
		System.out.println("ridadeSummad()");
		System.out.println(ridadeSummad(neo));
		
		System.out.println("korvalDiagonaaliSumma()");
		System.out.println(korvalDiagonaaliSumma(neo));
		
		System.out.println("ridadeMaksimumid()");
		System.out.println(ridadeMaksimumid(neo));
		
		System.out.println("miinimum()");
		System.out.println(miinimum(neo));
		
		System.out.println("kahegaJaakMaatriks()");
		System.out.println(kahegaJaakMaatriks(5,5));
		
		System.out.println("transponeeri()");
		System.out.println(transponeeri(neo));
	}

	public static void tryki(int[] massiiv) {
		String s = "";
		for (int i = 0; i < massiiv.length; i++) {
			s += Integer.toString(massiiv[i]) + " ";
		}
		System.out.println(s);
	}

	public static void trykiKahene(int[][] maatriks) {
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]);
		}
	}

	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad = new int[maatriks.length];

		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				summad[i] += maatriks[i][j];
			}
		}

		return summad;
	}

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;

		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				if (j == (maatriks.length - i - 1)) summa += maatriks[i][j];
			}
		}

		return summa;
	}

	public static int[] ridadeMaksimumid(int[][] maatriks) {
		int[] maksimumid = new int[maatriks.length];

		for (int i = 0; i < maatriks.length; i++) {
			maksimumid[i] = Integer.MIN_VALUE;
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] > maksimumid[i]) maksimumid[i] = maatriks[i][j];
			}
		}

		return maksimumid;
	}

	public static int miinimum(int[][] maatriks) {
		int miinimum = Integer.MAX_VALUE;
		
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] < miinimum) miinimum = maatriks[i][j];
			}
		}

		return miinimum;
	}
	
	public static int[][] kahegaJaakMaatriks(int ridu, int veerge) {
		int[][] maatriks = new int[ridu][veerge];
		
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				maatriks[i][j] = (i+j)%2;
			}
		}
		
		return maatriks;
	}
	
	public static int[][] transponeeri(int[][] maatriks) {
		int[][] transponeeritud = new int[maatriks[0].length][maatriks.length];
		
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				transponeeritud[j][i] = maatriks[i][j];
			}
		}
		
		return transponeeritud;
	}

}
