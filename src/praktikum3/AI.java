package praktikum3;
import lib.TextIO;

public class AI {
	public static void main(String[] args) {
		System.out.println("Sisesta 1. vanus:");
		int vanus1 = TextIO.getlnInt();
		while (vanus1 < 0) {
			System.out.println("Negatiivset vanust ei saa olla. Sisesta uuesti:");
			vanus1 = TextIO.getlnInt();
		}
		
		System.out.println("Sisesta 2. vanus:");
		int vanus2 = TextIO.getlnInt();
		while (vanus2 < 0) {
			System.out.println("Negatiivset vanust ei saa olla. Sisesta uuesti:");
			vanus2 = TextIO.getlnInt();
		}
		
		int vahe = Math.abs(vanus1 - vanus2);
		
		if (vahe > 10) {
			System.out.println("Midagi veel krõbedamat!!");
		} else if (vahe > 5) {
			System.out.println("Midagi krõbedat!");
		} else if (vahe < 5) {
			System.out.println("Sobib");
		}
	}
}