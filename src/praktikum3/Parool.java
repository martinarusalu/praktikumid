package praktikum3;
import lib.TextIO;

public class Parool {
	public static void main(String[] args) {
		String parool = "Passw0rd";
		System.out.println("Sisesta parool:");
		String sisestatudParool = TextIO.getlnString();
		
		while (!sisestatudParool.equals(parool)) {
			System.out.println("Vale parool! Sisesta uuesti:");
			sisestatudParool = TextIO.getlnString();
		}
		System.out.println("Õige parool");
	}
}