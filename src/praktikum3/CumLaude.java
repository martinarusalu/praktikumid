package praktikum3;
import lib.TextIO;

public class CumLaude {
	public static void main(String[] args) {
		// Keskmine hinne
		System.out.println("Sisesta keskmine hinne:");
		double keskmineHinne = TextIO.getlnDouble();
		while (keskmineHinne > 5 ||  keskmineHinne < 0) {
			System.out.println("Vigane hinne, sisesta uuesti:");
			keskmineHinne = TextIO.getlnDouble();
		}
		
		// Lõputöö hinne
		System.out.println("Sisesta lõputöö hinne:");
		double loputooHinne = TextIO.getlnDouble();
		while (loputooHinne > 5 ||  loputooHinne < 1) {
			System.out.println("Vigane hinne, sisesta uuesti:");
			loputooHinne = TextIO.getlnDouble();
		}
		
		// Cum laude test
		if (keskmineHinne > 4.5 && loputooHinne == 5) {
			System.out.println("Jah saad cum laude diplomile!");
		} else {
			System.out.println("Ei saa!");
		}
	}
}