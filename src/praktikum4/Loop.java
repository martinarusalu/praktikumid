package praktikum4;
import lib.TextIO;

public class Loop {
	public static void main(String[] args) {
		// 1. ülesanne:
		for (int i = 10; i > 0; i--) {
			System.out.print(i + " ");
		}
		
		// 2. ülesanne:
		System.out.println();
		for (int i = 0; i < 11; i += 2) {
			System.out.print(i + " ");
		}
		
		// 3. ülesanne:
		System.out.println();
		for (int i = 30; i >= 0; i--) {
			if (i % 3 == 0) {
				System.out.print(i + " ");
			}
		}
		
		// 4. ülesanne:
		System.out.println();
		int kuljeSuurus = 7;
		for (int i = 0; i < kuljeSuurus; i++) {
			for (int j = 0; j < kuljeSuurus; j++) {
				if (i == j) {
					System.out.print("1 ");
				} else {
					System.out.print("0 ");
				}
			}
			System.out.println();
		}
		
		// 5. ülesanne:
		System.out.println("Sisesta küljesuurus:");
		int kuljeSuurus2 = TextIO.getlnInt();
		String kriipsud = "";
		for (int i = 0; i < (kuljeSuurus2 + 2); i++) {
			if (i == kuljeSuurus2) kriipsud += "-";
			else kriipsud += "--";
		}
		System.out.println(kriipsud);
		for (int rida = 0; rida < kuljeSuurus2; rida++) {
			System.out.print("| ");
			for (int tulp = 0; tulp < kuljeSuurus2; tulp++) {
				if (tulp == rida || tulp == (kuljeSuurus2 - (rida+1))) {
					System.out.print("x ");
				} else {
					System.out.print("0 ");
				}
			}
			System.out.print("|\n");
		}
		System.out.println(kriipsud);
		
		// 6. ülesanne:
		int n = 0;
		for (int i = 0; i < 11; i++) {
			for (int j = 0; j < 11; j++) {
				if ((i + j) < 10) n = i + j;
				else if ((i + j) < 20) n = i + j - 10;
				else n = i + j - 20;
				System.out.print(n + " ");
			}
			System.out.println();
		}
		
		// Pangakaart:
		for (int i = 0; i < 10000; i++) {
			System.out.println(String.format("%04d", i));
		}
	}
}
