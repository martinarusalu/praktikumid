package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class Faililuger {

	public static void main(String[] args) {
	    ArrayList<String> list = getRead("tekstifail.txt");
	    Collections.sort(list);
	    
	    for (int i = 0; i < list.size(); i++) {
	    	System.out.println(list.get(i));
	    }
	}
	
	public static ArrayList<String> getRead (String failinimi) {
		ArrayList<String> read = new ArrayList<String>();
		
		String kataloogitee = Faililuger.class.getResource(".").getPath();
		File file = new File(kataloogitee + failinimi);
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;
			while ((rida = in.readLine()) != null) {
				read.add(rida.toUpperCase());
			}
			in.close();
		}
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		
		return read;
	}

}
