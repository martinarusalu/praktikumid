package praktikum13;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@SuppressWarnings("serial")
public class Klient implements java.io.Serializable {

	String nimi;
	int vanus;
	int kinganumber;

	public Klient(String nimi, int vanus, int kinganumber) {
		this.nimi = nimi;
		this.vanus = vanus;
		this.kinganumber = kinganumber;
	}

	public void kirjuta() {
		try {
			FileOutputStream f_out = new FileOutputStream(this.nimi + ".data");
			ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
			obj_out.writeObject(this);
			obj_out.close();
		} catch (Exception x) {
			System.out.println("Midagi läks valesti!");
		}
	}

	public static Klient getByNimi(String nimi) {
		try {
			FileInputStream f_in = new FileInputStream(nimi + ".data");
			ObjectInputStream obj_in = new ObjectInputStream(f_in);
			Object obj = obj_in.readObject();
			obj_in.close();
			if (obj instanceof Klient) {
				Klient klient = (Klient) obj;
				return klient;
			}
		} catch (Exception x) {
			System.out.println(x.getMessage());
		}
		return null;
	}

	public void kustuta() {
		File f = new File(this.nimi + ".data");
		if (f.delete()) {
			System.out.println("Kustutatud!");
		} else {
			System.out.println("Kustutamine ei õnnestunud!");
		}
	}

}
