package praktikum13;

import java.util.ArrayList;

public class Maatriksluger {

	public static void main(String[] args) {
		ArrayList<String> read = Faililuger.getRead("maatriks.txt");
		int[][] maatriks = getMaatriks(read);
		maatriks = transponeeri(maatriks);
		trykiMaatriks(maatriks);
	}
	
	public static int[][] getMaatriks (ArrayList<String> read) {
		String[] esimeneRida = read.get(0).split(",");
		int pikkus = esimeneRida.length;
		
		int[][] maatriks = new int[read.size()][pikkus];
		
		for (int i = 0; i < read.size(); i++) {
			String[] elemendid = read.get(i).split(",");
			for (int j = 0; j < elemendid.length; j++) {
				maatriks[i][j] = Integer.parseInt(elemendid[j]);
			}
		}
		
		return maatriks;
	}
	
	public static void trykiMaatriks (int[][] maatriks) {
		for (int i = 0; i < maatriks.length; i++) {
			String rida = "";
			for (int j = 0; j < maatriks[i].length; j++) {
				rida += maatriks[i][j] + " ";
			}
			System.out.println(rida);
		}
	}
	
	public static int[][] transponeeri(int[][] maatriks) {
		int[][] transponeeritud = new int[maatriks[0].length][maatriks.length];
		
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				transponeeritud[j][i] = maatriks[i][j];
			}
		}
		
		return transponeeritud;
	}

}
