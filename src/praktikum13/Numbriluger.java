package praktikum13;

import java.util.ArrayList;

public class Numbriluger {

	public static void main(String[] args) {
		ArrayList<String> numbriList = Faililuger.getRead("numbrid.txt");
		double keskmine = getKeskmine(numbriList);
		System.out.println(keskmine);
	}
	
	public static double getKeskmine (ArrayList<String> list) {
		double keskmine = 0;
		double summa = 0;
		
		for (String number : list) {
			summa += Integer.parseInt(number);
		}
		
		keskmine = summa / (double)list.size();
		
		return keskmine;
	}

}
