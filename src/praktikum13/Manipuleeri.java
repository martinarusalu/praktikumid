package praktikum13;

public class Manipuleeri {

	public static void main(String[] args) {
		kustutaKlient();
	}
	
	public static void looKlient() {
		Klient klient = new Klient("Martin", 20, 42);
		klient.kirjuta();
	}
	
	public static void loeKlient() {
		Klient klient = Klient.getByNimi("Martin");
		System.out.println(klient.vanus);
	}
	
	public static void kustutaKlient() {
		Klient klient = new Klient("Martin", 20, 42);
		klient.kirjuta();
		klient.kustuta();
	}

}
