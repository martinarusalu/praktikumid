package praktikum9;
import java.util.*;
import lib.TextIO;

public class TulpDiagramm {

	public static void main(String[] args) {
		System.out.println("Sisesta positiivne täisarv:");
		ArrayList<Integer> arvud = new ArrayList<Integer>();
		arvud = sisestaArvud(arvud);
		prindiArvud(arvud);
	}
	
	public static ArrayList<Integer> sisestaArvud (ArrayList<Integer> arvud) {
		int arv = TextIO.getlnInt();
		while (arv != 0) {
			if (arv < 0) System.out.println("Sisesta positiivne täisarv:");
			else {
				arvud.add(arv);
				System.out.println("Sisesta veel mõni positiivne täisarv:");
			}
			arv = TextIO.getlnInt();
		}
		
		return arvud;
	}
	
	public static void prindiArvud(ArrayList<Integer> arvud) {
		int suurim = Collections.max(arvud);
		float jagaja = (float)10 / (float)suurim;
		for (int i = 10; i > 0; i--) {
			String x = "";
			for (int arv : arvud) {
				if ((arv*jagaja) >= i) x += " x";
				else x+= "  ";
			}
			System.out.println(x);
		}
		
		String dash = "";
		for (int j = 0; j < arvud.size(); j++) {
			dash += "--";
		}
		System.out.println(dash);
		
		String arvudTekst = "";
		for (int arv : arvud) {
			// Eeldan, et arvud on maksimaalselt kahekohalised
			if (arv >= 10) arvudTekst += arv;
			else arvudTekst += " " + arv;
		}
		System.out.println(arvudTekst);
	}
}
