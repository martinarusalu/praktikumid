package praktikum9;

public class Massiiv2 {

	public static void main(String[] args) {
		int[][] neo = {
			    {1, 3, 6, 7},
			    {2, 3, 3, 1},
			    {17, 4, 5, 0},
			    {-20, 13, 16, 17}
			};
		System.out.println(suurim(neo));
	}
	
	public static int suurim (int[][] m) {
		int suurim = Integer.MIN_VALUE;
		
		for (int[] i : m) {
			int reaSuurim = Massiiv1.suurim(i);
			if (reaSuurim > suurim) suurim = reaSuurim;
		}
		
		return suurim;
	}

}
