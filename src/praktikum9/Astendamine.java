package praktikum9;

public class Astendamine {

	public static void main(String[] args) {
		System.out.println(astenda(2,4));
		System.out.println(astenda(5,3));
	}
	
	public static int astenda(int alus, int astendaja) {
		if (astendaja == 0) {
			return 1;
		} else {
			return alus * astenda(alus, astendaja-1);
		}
	}

}
