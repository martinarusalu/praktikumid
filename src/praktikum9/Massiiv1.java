package praktikum9;

public class Massiiv1 {

	public static void main(String[] args) {
		int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
		
		System.out.println(suurim(massiiv));
	}
	
	public static int suurim (int[] m) {
		int suurim = Integer.MIN_VALUE;
		
		for (int i : m) {
			if (i > suurim) suurim = i;
		}
		
		return suurim;
	}

}
