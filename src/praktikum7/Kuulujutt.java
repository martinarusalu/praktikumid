package praktikum7;

public class Kuulujutt {

	public static void main(String[] args) {
		int len = 4;
		
		String[] naisenimed = {"Maie", "Mari", "Kirsi", "Agnes"};
		String[] mehenimed = {"Mart", "Kalle", "Toomas", "Peeter"};
		String[] tegusonad = {"Töötavad", "Mängivad", "Püüavad kala", "Õpivad"};
		
		String randNaisenimi = naisenimed[(int)Math.floor(Math.random()*len)];
		String randMehenimi = mehenimed[(int)Math.floor(Math.random()*len)];
		String randTegusona = tegusonad[(int)Math.floor(Math.random()*len)];
		
		System.out.println(randNaisenimi + " ja " + randMehenimi + " " + randTegusona + ".");
	}

}
