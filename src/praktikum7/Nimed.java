package praktikum7;
import lib.TextIO;
import java.util.*;

public class Nimed {

	public static void main(String[] args) {
		ArrayList<String> nimed = new ArrayList<String>();
		System.out.println("Sisesta nimed:");
		String nimi = TextIO.getlnString();
		
		while (!nimi.equals("")) {
			nimed.add(nimi);
			nimi = TextIO.getlnString();
		}
		
		Collections.sort(nimed);
		
		for (String nimiMassiivis : nimed) {
		    System.out.println(nimiMassiivis);
		}
	}
}
