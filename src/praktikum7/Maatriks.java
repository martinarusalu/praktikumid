package praktikum7;
import java.util.*;

public class Maatriks {

	public static void main(String[] args) {
		int[][] maatriks1 = {{1,2,3},{4,5,6},{10000,7,8},{9,10,11},{12,13,14,15,16}};
		int[][] maatriks2 = {{0},{-1,2}};
		
		System.out.println(maksimumMaatriks(maatriks1));
		System.out.println(maksimumMaatriks(maatriks2));
	}
	
	public static int maksimumMassiiv (int[] massiiv) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < massiiv.length; i++) {
			list.add(massiiv[i]);
		}
		int maks = Collections.max(list);
		return maks;
	}
	
	public static int maksimumMaatriks(int[][] maatriks) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < maatriks.length; i++) {
			list.add(maksimumMassiiv(maatriks[i]));
		}
		int maks = Collections.max(list);
		return maks;
	}
}
