package praktikum7;
import lib.TextIO;

public class Arve10 {

	public static void main(String[] args) {
		int[] arvud = new int[10];
		System.out.println("Sisesta 10 arvu:");
		for (int i = 0; i < 10; i++) {
			arvud[i] = TextIO.getlnInt();
		}
		for (int j = arvud.length; j > 0; j--) {
			System.out.println(arvud[j-1]);
		}
	}

}
