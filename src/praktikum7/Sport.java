package praktikum7;
import java.util.*;
import lib.TextIO;
import praktikum7.Sportlane;

public class Sport {

	public static void main(String[] args) {
		Sportlane[] sportlased = new Sportlane[10];
		
		for (int i = 0; i < 10; i++) {
			System.out.println("Sisesta " + (i+1) + ". nimi:");
			String nimi = TextIO.getlnString();
			System.out.println("Sisesta " + (i+1) + ". sportlase esimene tulemus:");
			double tulemus1 = TextIO.getlnDouble();
			System.out.println("Sisesta " + (i+1) + ". sportlase teine tulemus:");
			double tulemus2 = TextIO.getlnDouble();
			
			sportlased[i] = new Sportlane(nimi,tulemus1,tulemus2);
		}
		
		System.out.println("Mille järgi sorteerida?");
		String sortimine = TextIO.getlnString().toLowerCase();
		while (!sortimine.equals("nimi") && !sortimine.equals("tulemus1") && !sortimine.equals("tulemus2")) {
			System.out.println("Sisesta kas \"nimi\", \"tulemus1\" või \"tulemus2\":");
			sortimine = TextIO.getlnString();
		}
		
		System.out.println("Kas sorteerida kasvavas või kahanevas järjekorras?");
		String kasvKahan = TextIO.getlnString().toLowerCase();
		while (!kasvKahan.equals("kasvav") && !kasvKahan.equals("kahanev")) {
			System.out.println("Sisesta kas \"kasvav\" või \"kahanev\":");
			kasvKahan = TextIO.getlnString().toLowerCase();
		}
		
		List<Sportlane> list = new ArrayList<Sportlane>();
		// TODO sorteerimine
		/*if (sortimine.equals("nimi")) {
			Collections.sort(list, new Comparator<Sportlane>(){
			    public int compare(Sportlane s1, Sportlane s2) {
			        return s1.getNimi().compareToIgnoreCase(s2.getNimi());
			    }
			});
		} else if (sortimine.equals("tulemus1")) {
			Collections.sort(list, new Comparator<Sportlane>(){
			    public int compare(Sportlane s1, Sportlane s2) {
			    	if (s1.getTulemus1() < s2.getTulemus1()) {
			    		if (kasvKahan.equals("kasvav")) return -1;
			    		else return 1; 
			    	}
			        if (s1.getTulemus1() > s2.getTulemus1()) {
			        	if (kasvKahan.equals("kasvav")) return 1;
			        	else return -1;
			        }
			        return 0;
			    }
			});
		} else {
			Collections.sort(list, new Comparator<Sportlane>(){
			    public int compare(Sportlane s1, Sportlane s2) {
			    	if (s1.getTulemus2() < s2.getTulemus2()) {
			    		if (kasvKahan.equals("kasvav")) return -1;
			    		else return 1; 
			    	}
			        if (s1.getTulemus2() > s2.getTulemus2()) {
			        	if (kasvKahan.equals("kasvav")) return 1;
			        	else return -1;
			        }
			        return 0;
			    }
			});
		}*/
	}
}
