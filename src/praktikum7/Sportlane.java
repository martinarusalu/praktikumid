package praktikum7;

public class Sportlane implements Comparable<Sportlane> {
	String nimi;
	double tulemus1;
	double tulemus2;
	
	public Sportlane(String nimi, double tulemus1, double tulemus2) {
		this.nimi = nimi;
		this.tulemus1 = tulemus1;
		this.tulemus2 = tulemus2;
	}
	
	public String getNimi () {
		return this.nimi;
	}
	
	public void setNimi (String nimi) {
		this.nimi = nimi;
	}
	
	public double getTulemus1 () {
		return this.tulemus1;
	}
	
	public void setTulemus1 (double tulemus1) {
		this.tulemus1 = tulemus1;
	}
	
	public double getTulemus2 () {
		return this.tulemus2;
	}
	
	public void setTulemus2 (double tulemus2) {
		this.tulemus2 = tulemus2;
	}
	
	public int compareTo(Sportlane s)
    {
        return getNimi().compareTo(s.getNimi());
    }
}
