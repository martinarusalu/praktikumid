package praktikum6;
import lib.TextIO;

public class Taring {

	public static void main(String[] args) {
		int kasutajaRaha = 100;
		while(kasutajaRaha > 0) {
			System.out.println("Raha: " + kasutajaRaha);
			System.out.println("Mis number tuleb?");
			int pakkumine = TextIO.getlnInt();
			while (pakkumine < 1 || pakkumine > 6) {
				System.out.println("Vigane pakkumine. sisesta uuesti:");
				pakkumine = TextIO.getlnInt();
			}
			System.out.println("Panuse suurus:");
			int panus = TextIO.getlnInt();
			while (panus <= 0 || panus > 25 || kasutajaRaha - panus < 0) {
				System.out.println("Vigane panus. sisesta uuesti:");
				panus = TextIO.getlnInt();
			}
			kasutajaRaha -= panus;
			int tulemus = taring();
			if (pakkumine == tulemus) {
				kasutajaRaha += panus*6;
				System.out.println("Õige!");
			} else {
				System.out.println("Vale. Tegelikult oli: " + tulemus);
			}
			if (kasutajaRaha == 0) {
				System.out.println("Raha otsas.");
				break;
			}
		}
	}
	
	public static int taring() {
		int number = Arvamismang.suvalineArv(1, 6);
		return number;
	}

}
