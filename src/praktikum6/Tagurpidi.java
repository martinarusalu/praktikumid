package praktikum6;
import lib.TextIO;

public class Tagurpidi {

	public static void main(String[] args) {
		System.out.println("Sõna:");
		String sona = TextIO.getlnString();
		String tagurpidi = new StringBuilder(sona).reverse().toString();
		System.out.println(tagurpidi);
	}

}
