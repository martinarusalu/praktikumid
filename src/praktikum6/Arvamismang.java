package praktikum6;
import lib.TextIO;

public class Arvamismang {
	public static void main(String[] args) {
		int arv = suvalineArv(1,10000);
		System.out.println("Mis number oli?");
		int pakkumine = TextIO.getlnInt();
		boolean vale = true;
		while (vale) {
			if (pakkumine > arv) {
				System.out.println("Väiksem.");
				pakkumine = TextIO.getlnInt();
			} else if (pakkumine < arv) {
				System.out.println("Suurem.");
				pakkumine = TextIO.getlnInt();
			}
			else if (pakkumine == arv) {
				System.out.println("Õge!");
				vale = false;
			}
		}
	}
	
	public static int suvalineArv(int min, int max) {
		int suvaline = min + (int)(Math.random() * ((max - min) + 1));
		return suvaline;
	}
}
