package praktikum6;
import lib.TextIO;

public class KullKiri {

	public static void main(String[] args) {
		int kasutajaRaha = 100;
		System.out.println("Panuse suurus:");
		int panus = TextIO.getlnInt();
		while(kasutajaRaha > 0) {
			while (panus <= 0 || panus > 25 || kasutajaRaha - panus < 0) {
				System.out.println("Vigane panus. sisesta uuesti:");
				panus = TextIO.getlnInt();
			}
			if (kiri()) {
				kasutajaRaha += panus*2;
				System.out.println("Kiri!");
			} else {
				kasutajaRaha -= panus;
				System.out.println("Kull!");
			}
			if (kasutajaRaha == 0) {
				System.out.println("Raha otsas.");
				break;
			} else {
				System.out.println("Raha: " + kasutajaRaha);
				System.out.println("Uus panus");
				panus = TextIO.getlnInt();
			}
		}
	}
	
	public static boolean kiri() {
		boolean kiri = false;
		if (Math.random() > 0.5) kiri = true;
		return kiri;
	}

}
