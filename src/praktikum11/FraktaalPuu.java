package praktikum11;

import java.applet.Applet;
import java.awt.*;

@SuppressWarnings("serial")
public class FraktaalPuu extends Applet {
    @Override
    public void paint(Graphics g) {
        int w = getWidth();
        int h = getHeight();

        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);

        g.setColor(Color.black);
        
        int x = w/2;
        int y = (int)(h*0.8);
        
        puu(g, x, y, -90, 10);
    }
    
    private void puu(Graphics g, int x1, int y1, double nurk, int tase) {
        if (tase == 0) return;
        
        int x2 = x1 + (int) (Math.cos(Math.toRadians(nurk)) * tase * (Math.random()*10+5) * 0.5);
        int y2 = y1 + (int) (Math.sin(Math.toRadians(nurk)) * tase * (Math.random()*10+5) * 0.5);
        
        g.drawLine(x1, y1, x2, y2);
        
        puu(g, x2, y2, nurk - 20, tase - 1);
        puu(g, x2, y2, nurk + 20, tase - 1);
    }
}
