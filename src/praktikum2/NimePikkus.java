package praktikum2;
import lib.TextIO;

public class NimePikkus {
	public static void main(String[] args) {
		System.out.println("Sisesta oma nimi:");
		String nimi = TextIO.getlnString();
		System.out.println("Sinu nimes on " + nimi.length() + " tähte.");
	}
}