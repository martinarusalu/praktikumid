package praktikum2;
import lib.TextIO;

public class Grupid {
	public static void main(String[] args) {
		System.out.println("Sisesta inimeste arv:");
		int inimesed = TextIO.getlnInt();
		System.out.println("Sisesta grupi suurus:");
		int grupiSuurus = TextIO.getlnInt();
		int gruppideArv = inimesed / grupiSuurus;
		int jaak = inimesed % grupiSuurus;
		System.out.println(inimesed + " inimesest saab moodustada " + gruppideArv + " gruppi.");
		System.out.println("Üle jääb " + jaak + " inimest.");
	}
}