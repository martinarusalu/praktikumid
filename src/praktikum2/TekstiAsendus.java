package praktikum2;
import lib.TextIO;

public class TekstiAsendus {
	public static void main(String[] args) {
		System.out.println("Sisesta midagi:");
		String text = TextIO.getlnString();
		System.out.println(text.replace("a", "_"));
	}
}