package praktikum2;
import lib.TextIO;

public class Korrutis {
	public static void main(String[] args) {
		System.out.println("Sisesta esimene number:");
		int esimene = TextIO.getlnInt();
		System.out.println("Sisesta teine number:");
		int teine = TextIO.getlnInt();
		System.out.println(esimene + " * " + teine + " = " + esimene * teine);
	}
}